// ignore_for_file: avoid_print

import 'package:flutter/material.dart';

import 'screens/SetTimerScreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Viet Nam Time Counter App',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const SetTimerScreen(title: 'Set Timer Screen'),
      debugShowCheckedModeBanner: false,
    );
  }
}
