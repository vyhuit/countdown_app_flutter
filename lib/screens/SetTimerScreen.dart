// ignore_for_file: avoid_print, file_names

import 'package:flutter/material.dart';

class SetTimerScreen extends StatefulWidget {
  const SetTimerScreen({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<SetTimerScreen> createState() => _SetTimerScreenState();
}

class _SetTimerScreenState extends State<SetTimerScreen> {
  int _hour = 0;
  int _minute = 0;
  int _seconds = 0;
  bool isStopped = true;

  final hourController = TextEditingController();
  final minuteController = TextEditingController();
  final secondsController = TextEditingController();

  void hourInputChange() {
    try {
      int input =
          hourController.text == "" || int.parse(hourController.text).isNaN
              ? 0
              : int.parse(hourController.text);
      _hour = input;
    } catch (e) {
      print("Loi typo");
      hourController.text = _hour.toString();
    }
  }

  void minuteInputChange() {
    try {
      int input =
          minuteController.text == "" || int.parse(minuteController.text).isNaN
              ? 0
              : int.parse(minuteController.text);
      _minute = input;
    } catch (e) {
      print("Loi typo");
      minuteController.text = _minute.toString();
    }
  }

  void secondsInputChange() {
    try {
      int input = secondsController.text == "" ||
              int.parse(secondsController.text).isNaN
          ? 0
          : int.parse(secondsController.text);
      _seconds = input;
    } catch (e) {
      print("Loi typo");
      secondsController.text = _seconds.toString();
    }
  }

  void timerStop() {
    setState(() {
      isStopped = true;
    });
  }

  Future<void> timerStart() async {
    int _total = _hour * 3600 + _minute * 60 + _seconds;
    if (_total > 0) {
      setState(() {
        isStopped = false;
      });
      while (_total >= 0) {
        if (!isStopped) {
          if (_total > 0) {
            _total -= 1;
            await Future.delayed(const Duration(seconds: 1)).then((_) {
              _hour = _total ~/ 3600;
              _minute = (_total - _hour * 3600) ~/ 60;
              _seconds = (_total - _hour * 3600 - _minute * 60);
              hourController.text = _hour.toString();
              minuteController.text = _minute.toString();
              secondsController.text = _seconds.toString();
            });
          } else {
            setState(() {
              isStopped = true;
            });
            secondsController.text = "0";
            showDialog<String>(
                context: context,
                builder: (BuildContext context) => AlertDialog(
                        title: const Text(''),
                        content: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const <Widget>[Text('Time is out')],
                        ),
                        actions: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              TextButton(
                                onPressed: () {
                                  setState(() {
                                    isStopped = true;
                                  });
                                  Navigator.pop(context, 'OK');
                                },
                                child: const Text('OK'),
                              ),
                            ],
                          ),
                        ]));
          }
        } else {
          setState(() {
            isStopped = true;
          });
          hourController.text = "0";
          minuteController.text = "0";
          secondsController.text = "0";
          break;
        }
      }
    }
  }

  @override
  void initState() {
    super.initState();

    hourController.addListener(hourInputChange);
    minuteController.addListener(minuteInputChange);
    secondsController.addListener(secondsInputChange);
  }

  @override
  void dispose() {
    hourController.dispose();
    minuteController.dispose();
    secondsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(50, 5, 50, 50),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextFormField(
                      controller: hourController,
                      enabled: isStopped ? true : false,
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        label: Center(child: Text("Hour")),
                      ),
                    ),
                  ),
                  Expanded(
                    child: TextFormField(
                      enabled: isStopped ? true : false,
                      controller: minuteController,
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        label: Center(child: Text("Minute")),
                      ),
                    ),
                  ),
                  Expanded(
                    child: TextFormField(
                      controller: secondsController,
                      enabled: isStopped ? true : false,
                      keyboardType: TextInputType.number,
                      textAlign: TextAlign.center,
                      decoration: const InputDecoration(
                        border: UnderlineInputBorder(),
                        label: Center(child: Text("Seconds")),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: [
                TextButton(
                  child: const Text('Stop', style: TextStyle(fontSize: 20.0)),
                  onPressed: isStopped ? null : timerStop,
                ),
                TextButton(
                  child: const Text(
                    'Start',
                    style: TextStyle(fontSize: 20.0),
                  ),
                  onPressed: isStopped ? timerStart : null,
                )
              ],
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
            ),
          ],
        ),
      ),
    );
  }
}
